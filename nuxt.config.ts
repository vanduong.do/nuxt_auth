// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devtools: { enabled: true },
    modules: [
        "@ant-design-vue/nuxt",
        '@pinia/nuxt',
        '@pinia-plugin-persistedstate/nuxt',
    ],
    antd: {
        // Options
    },
    plugins: [
        { src: '~/plugins/cookies.ts' },
        { src: '~/plugins/axios.ts' },
    ],
    runtimeConfig: {
        // The private keys which are only available within server-side
        // apiSecret: '',
        // Keys within public, will be also exposed to the client-side
        public: {
            apiBase: process.env.BASE_URL,
        },
        
    },
    pinia: {
        storesDirs: ['./stores/**'],
    },
    imports: {
        dirs: ['./stores/**'],
    },
    ssr: false,
});
