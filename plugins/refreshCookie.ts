import { useAuthStore } from "~/stores/authStore"

export default defineNuxtPlugin((nuxtApp) => {
    const authStore = useAuthStore();

    return {
        provide: {
            refreshCookie: async (token: string) => await authStore.refreshToken(token)
        }
      }
})
