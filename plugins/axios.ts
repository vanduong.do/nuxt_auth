import axios, { type AxiosRequestConfig } from "axios";
import { apiInstance } from "~/composables/api";

export default defineNuxtPlugin(({ app }) => {
    let isRefreshing = false;
    const instance = apiInstance();
    const router = useRouter();
    const { $cookies } = useNuxtApp();

    const userStore = useUserStore();
    const { user } = storeToRefs(userStore);

    const requestIntercept = instance.interceptors.request.use(
        (config) => {
            // Do something before request is sent
            const cookie = useCookie<{ name: string }>("ACCESS_TOKEN");
            if (cookie) {
                config.headers["Authorization"] = `Bearer ${cookie.value}`;
            }
            return config;
        },
        (error) => {
            // Do something with request error
            return Promise.reject(error);
        }
    );
    const responseIntercept = instance.interceptors.response.use(
        (response) => {
            // Do something with response data
            return response;
        },
        async (error) => {
            const originalConfig = error.config;

            // Do something with response error
            if (error.response?.status === 401 && error.response?.data?.message === "Token is Expired" && !originalConfig._retry && !isRefreshing) {
                originalConfig._retry = true;

                isRefreshing = true;
                const cookie: any = useCookie<{ name: string }>("REFRESH_TOKEN");
                if (cookie) {
                    await instance
                        .post(
                            "refresh",
                            { refresh_token: cookie.value },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                },
                            }
                        )
                        .then((res: any) => {
                            // isRefreshing = false;
                            let data = res.data;
                            $cookies.set("ACCESS_TOKEN", data.access_token, "1d");
                            $cookies.set("REFRESH_TOKEN", data.refresh_token, "1d");
                            // return instance(originalConfig);
                        }).catch((): void => {
                            $cookies.remove("ACCESS_TOKEN");
                            $cookies.remove("REFRESH_TOKEN");
                            navigateTo("/login", { external: true });
                        });
                }
            }
            isRefreshing = false;
            // console.log("error.response?.status", error.response?.status);
            // Promise.reject(error); // Tại lỗi ở đây ko thể đi vào function getUser() trong authStore.ts
            return instance(originalConfig);
        }
    );

    return {
        provide: { axios: instance },
    };
});
