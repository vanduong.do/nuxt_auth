import { useUserStore } from "~/stores/userStore"

export default defineNuxtPlugin(async (nuxtApp) => {
    const authStore = useAuthStore();
    
    const cookie = useCookie<{ name: string }>("ACCESS_TOKEN");
    
    if(!authStore.isLoggedIn && cookie.value){
        await authStore.getUser();
    }
})
