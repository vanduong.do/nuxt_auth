import { useCookies } from "vue3-cookies";

export default defineNuxtPlugin(nuxtApp => {
  const { cookies } = useCookies();
    
    return {
      provide: {
        cookies: cookies
      }
    }
});