import { defineStore } from "pinia";
import axios from "axios";

type User = {
    id: number;
    name: string;
    email: string;
};

export const useUserStore = defineStore("user", () => {
    const user = ref<User | null>(null);
    const isLoaded = ref<boolean>(false);
    const email = ref<string | null>(null);

    return { user, isLoaded, email };
});
