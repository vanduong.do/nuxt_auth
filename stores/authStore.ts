import { defineStore } from "pinia";

type LoginCredentials = {
    email: string;
    password: string;
};

type RegisterCredentials = {
    username: string;
    email: string;
    password: string;
    password_confirmation: string;
};

type ResetPWCredentials = {
    token: string;
    password: string;
    password_confirmation: string;
};

export const useAuthStore = defineStore("auth", () => {
    const userStore = useUserStore();
    const { user, email } = storeToRefs(userStore);

    const { $axios } = useNuxtApp();
    const { $cookies } = useNuxtApp();

    const isLoggedIn = computed(() => !!user.value);
    const errorAuth = ref<string | null>(null);

    const cookie = useCookie<{ name: string }>("ACCESS_TOKEN");

    async function login(credentials: LoginCredentials) {
        await $axios
            .post("login", credentials, {
                headers: {
                    "Content-Type": "application/json",
                },
            })
            .then((res: any) => {
                if (res.status === 200) {
                    let data = res.data;
                    $cookies.set("ACCESS_TOKEN", data.access_token, "1d");
                    $cookies.set("REFRESH_TOKEN", data.refresh_token, "1d");
                }
            })
    }

    async function getUser() {
        await $axios.get("user-profile").then(response => {  
            // console.log("user-profile1111");
            // console.log(response);
            if (response.status == 200) {
                user.value = response.data;
            }
        })
        .catch((error: any) => {
            user.value = null;
            Promise.reject(error);
        });
    }

    async function register(credentials: RegisterCredentials) {
        await $axios.post("register", credentials, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${cookie.value}`
            },
        });
    }

    async function forgotPassword(email: string) {
        await $axios.post(
            "forgot-password",
            {
                email: email,
            },
            {
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );
    }

    async function verifyToken(token: string) {
        await $axios.get(`reset-password/${token}`).then((res: any) => {
            if (res.status === 200) {
                email.value = res.data.user.email;
            }
        });
    }

    async function resetPassword(credentials: ResetPWCredentials) {
        await $axios.post(`reset-password`, credentials, {
            headers: {
                "Content-Type": "application/json",
            },
        });
    }

    async function updatePassword(credentials: { old_password: string,password: string; password_confirmation: string }) {
        await $axios.post(`change-pass`, credentials, {
            headers: {
                "Content-Type": "application/json",
            },
        });
    }

    async function refreshToken(refreshToken: string) {
        await $axios.post("refresh-token", {refresh_token: refreshToken}, {
            headers: {
                "Content-Type": "application/json",
            },
        }).then((res: any) => {
            if (res.status === 200) {
                let data = res.data;
                console.log(data);
                
                $cookies.set("ACCESS_TOKEN", data.access_token, "1d");
                $cookies.set("REFRESH_TOKEN", data.refresh_token, "1d");
            }
        }).catch((error: any) => {
            Promise.reject(error);
        });
    }

    async function logout() {
        await $axios.post("logout");
        user.value = null;
        $cookies.remove("ACCESS_TOKEN");
        $cookies.remove("REFRESH_TOKEN");
        navigateTo("/login");
    }

    return { isLoggedIn, errorAuth, login, getUser, register, logout, forgotPassword, verifyToken, resetPassword, refreshToken, updatePassword }
})

