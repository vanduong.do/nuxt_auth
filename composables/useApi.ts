import axios, { Axios, type AxiosRequestConfig } from "axios";

export const useApiBridge = () => {
    const runtimeConfig = useRuntimeConfig();
    const router = useRouter();
    const defaultUrl = runtimeConfig.public.apiBase + "auth/";
    let api = axios.create({
        baseURL: defaultUrl,
        withCredentials: false,
    });

    api.interceptors.request.use(
        (config) => {
            // Do something before request is sent
            const cookie = useCookie<{ name: string }>("user");
            if (cookie) {
                config.headers["Authorization"] = `Bearer ${cookie.value}`;
            }
            return config;
        },
        (error) => {
            // Do something with request error
            return Promise.reject(error);
        }
    );
    api.interceptors.response.use(
        (response) => {
            // Do something with response data

            return response;
        },
        (error) => {
            // Do something with response error
            if (error.response.status === 401) {
                router.push({ path: "/login" });
            }
            console.log(error);

            return Promise.reject(error);
        }
    );

    return {
        register: async (data: { username: string ,email: string; password: string, password_confirmation: string }) => {
            return await api.post("register", data, {
                headers: {
                    "Content-Type": "application/json",
                },
            });
        },
        login: async (data: { email: string; password: string }) => {
            return await api.post("login", data, {
                headers: {
                    "Content-Type": "application/json",
                },
            });
        },
        logout: async () => {
            return await api.post("logout");
        },
    };
};
