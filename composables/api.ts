import axios, { Axios } from "axios";

export const apiInstance = () => {
    const runtimeConfig = useRuntimeConfig();

    const defaultUrl = runtimeConfig.public.apiBase + "auth/";

    let instance = axios.create({
        baseURL: defaultUrl,
        withCredentials: false,
        headers: {
            "Content-Type": "application/json",
        },
    });

    return instance;
};
